
- When using fedora-cloud as a base image `man` files are disabled by default
    - Drop `tsflags=nodocs` from `/etc/dnf/dnf.conf`

